package Utility;

import java.io.IOException;

public class SOR {
	
	

	public static  String Pg_Departments_Tbl_DepartmentCol = "Pg(Departments).Button(AddDepartment)";
	public static String Pg_Departments_Btn_AddDepartment = "Pg(Departments).Button(Save)";
	public static String Pg_Departments_Btn_Save = "Pg_Departments_Btn_Save";
	public static String Pg_Departments_Edt_Department = "Pg_Departments_Edt_Department";
	public static String Pg_Departments_Edt_EnterDepartment = "Pg_Departments_Edt_EnterDepartment";
	public static String Pg_Departments_Tab_Departments = "Pg_Departments_Tab_Departments";
	public static String Pg_Departments_Tab_Workgroups = "Pg_Departments_Tab_Workgroups";
	public static String Pg_Departments_Tbl_DelCol = "Pg_Departments_Tbl_DelCol";
	public static String Pg_Departments_Tbl_Header = "Pg_Departments_Tbl_Header";
	public static String Pg_Departments_Chk_ActiveCol = "Pg_Departments_Chk_ActiveCol";
	public static String Pg_Departments_Chk_ShowActiveOnly = "Pg_Departments_Chk_ShowActiveOnly";
	public static String Pg_Departments_Grd_Department = "Pg_Departments_Grd_Department";
	public static String Pg_Departments_TblHdr_Active = "Pg_Departments_TblHdr_Active";
	public static String Pg_Departments_TblHdr_Departments = "Pg_Departments_TblHdr_Departments";
	public static String Pg_Departments_TblHdr_From = "Pg_Departments_TblHdr_From";
	public static String Pg_Departments_TblHdr_To = "Pg_Departments_TblHdr_To";
	public static String Pg_Departments_Edt_Department2 = "Pg_Departments_Edt_Department2";
	public static String Pg_Departments_Edt_Department1 = "Pg_Departments_Edt_Department1";

	public static String Dlg_OK = "Dlg_OK";
	public static String Dlg_StaticTest = "Dlg_StaticTest";
	public static String Dlg_No = "Dlg_No";
	public static String Dlg_Yes = "Dlg_Yes";
	public static String LoginPage = "LoginPage";
	public static String Pg_Login_Btn_Login = "Pg_Login_Btn_Login";
	public static String Pg_Login_Edt_Name = "Pg_Login_Edt_Name";
	public static String Pg_Login_Edt_Password = "Pg_Login_Edt_Password";

	public static String Menu_DASHBOARD = "Menu_DASHBOARD";
	public static String Menu_EMPLOYEES = "Menu_EMPLOYEES";
	public static String Menu_PROJECTSETUP = "Menu_PROJECTSETUP";
	public static String Menu_REPORTS = "Menu_REPORTS";
	public static String Menu_TIMECARDBATCHES = "Menu_TIMECARDBATCHES";
	public static String Menu_TIME_ENTRY = "Menu_TIME_ENTRY";
	public static String Menu_TOOLS = "Menu_TOOLS";

	public static String Frm_BusyForm = "Frm_BusyForm";

	public static String LstMenu_ManageStartCards = "LstMenu_ManageStartCards";
	public static String LstMenu_Departments = "LstMenu_Departments";

	public static String Pg_SelectProject_Header_SelectProject = "Pg_SelectProject_Header_SelectProject";
	public static String Pg_SelectProject_Project_CHRONOS_TEST = "Pg_SelectProject_Project_CHRONOS_TEST";
	public static String Pg_SelectProject_Project_CONTRACTS = "Pg_SelectProject_Project_CONTRACTS";
	public static String Pg_SelectProject_Project_PAI_REG4 = "Pg_SelectProject_Project_PAI_REG4";
	public static String Pg_SelectProject_Project_TIMECARDS = "Pg_SelectProject_Project_TIMECARDS";
	public static String Pg_SelectProject_Project_TIMETRAX_QTP = "Pg_SelectProject_Project_TIMETRAX_QTP";

	public static String Pg_StartCardRight_Btn_Delete = "Pg_StartCardRight_Btn_Delete";
	public static String Pg_StartCardRight_Btn_Save = "Pg_StartCardRight_Btn_Save";
	public static String Pg_StartCardRight_Edt_FirstName = "Pg_StartCardRight_Edt_FirstName";
	public static String Pg_StartCardRight_Edt_LastName = "Pg_StartCardRight_Edt_LastName";
	public static String Pg_StartCardRight_Edt_SSN = "Pg_StartCardRight_Edt_SSN";
	public static String Pg_StartCardRight_Edt_WorkLocST = "Pg_StartCardRight_Edt_WorkLocST";
	public static String Pg_StartCardRight_Lst_Job = "Pg_StartCardRight_Lst_Job";
	public static String Pg_StartCardRight_Lst_Sched = "Pg_StartCardRight_Lst_Sched";
	public static String Pg_StartCardRight_Lst_Union = "Pg_StartCardRight_Lst_Union";
	public static String Pg_StartCardRight_Lst_Department = "Pg_StartCardRight_Lst_Department";
	public static String Pg_StartCardRight_Edt_WorkLocCTI = "Pg_StartCardRight_Edt_WorkLocCTI";
	public static String Pg_StartCardRight_Edt_WorkLocCTY = "Pg_StartCardRight_Edt_WorkLocCTY";

	public static String Pg_StartCardLeft_LastName_QTPAutoTest = "Pg_StartCardLeft_LastName_QTPAutoTest";
	public static String Tolbr__Btn_Project_Select = "Tolbr__Btn_Project_Select";

	
	public static String VerifyExcelReport() throws IOException
	{
		Logs.Ulog("Start VerifyExcelReport " );
		return TC.PASS;
	
		
		
	}
}
