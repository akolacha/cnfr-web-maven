package Utility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xmlbeans.XmlException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



// This class shows you that how to update a XML in Java
public class UpdateXmlValue {

    

     public String updateXML(String reqContent, String tagName, String strNewValue) {
          DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();

          try {
               // Convert CDATA nodes to Text node append
               docFactory.setCoalescing(true);

               DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
               Document document = docBuilder.parse(new InputSource(new StringReader(reqContent)));
             
               
               
               Element root = document.getDocumentElement();

               // Return the NodeList on a given tag name
               NodeList childNodes = root.getElementsByTagName(tagName);

               for(int index = 0; index < childNodes.getLength(); index++) {
                    NodeList subChildNodes = childNodes.item(index).getChildNodes();
                    if(subChildNodes.item(index).getTextContent().equals("?")) {
                         // Update the relevant node text content. item() position the NodeList.
                         subChildNodes.item(index).setTextContent(strNewValue);
                    }
               }
               
               TransformerFactory factory = TransformerFactory.newInstance();
               Transformer transformer = factory.newTransformer();
               transformer.setOutputProperty(OutputKeys.INDENT, "yes");

               StringWriter writer = new StringWriter();
               StreamResult result = new StreamResult(writer);
               DOMSource source = new DOMSource(document);

               transformer.transform(source, result);

               return writer.toString();
            
          }
          catch(Exception ex) {
          }
		return "null";
     }

    public static void main(String[] args) throws XmlException, IOException {
        
//          WsdlProject project = new WsdlProject();
//      	
//  		// import amazon wsdl
//  	
//  		WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(project, "http://wsf.cdyne.com/WeatherWS/Weather.asmx?WSDL", true )[0];
//  		
//  		// get desired operation
//  	
//  		WsdlOperation op = wsdl.getOperationAt(0);

  		

  		//String reqContent = op.createRequest(true);
          
          //new UpdateXmlValue().updateXML(reqContent, "weat:ZIP", "94701");
     }

	public String updateXML(String reqContent, HashMap sOAPKeys) throws SAXException, IOException, ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		
		   DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setCoalescing(true);

        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        Document document = docBuilder.parse(new InputSource(new StringReader(reqContent)));
      
        
        
        Element root = document.getDocumentElement();
for(int i= 0; i<sOAPKeys.size(); i++)
{

        // Return the NodeList on a given tag name
        NodeList childNodes = root.getElementsByTagName((String) sOAPKeys.keySet().toArray()[i]);

        
        
        for(int index = 0; index < childNodes.getLength(); index++) {
             NodeList subChildNodes = childNodes.item(index).getChildNodes();
             if(subChildNodes.item(index).getTextContent().equals("?")) {
                  // Update the relevant node text content. item() position the NodeList.
                  subChildNodes.item(index).setTextContent((String) sOAPKeys.get(sOAPKeys.keySet().toArray()[i]));
             }
        }

}
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(document);

        transformer.transform(source, result);

        return writer.toString();
	
	}
}