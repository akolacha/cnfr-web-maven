package Utility;

import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.StoreTable;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.thoughtworks.selenium.webdriven.commands.Click;

public class UserdefinedLibrary extends StandardLibrary {

	public static JavascriptExecutor js;
	public static String emailid = "";

	public UserdefinedLibrary() throws InterruptedException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static void Echo() {

		System.out.println("In Userdefined Function" + TC.InputData);

	}

	public static void EnterTExt() throws IOException, InterruptedException {

		TORObj().sendKeys(
				"\\% eval program_start=strptime(start_date, \"%Y-%m-%d\") \\% eval program_end =strptime(stop_date, \"%Y-%m-%d\") \\% eval search_earliest=relative_time(now(),\"@d\" )\\% eval search_latest=if(\"now\"=\"now\", now(), relative_time(now(),\"now\"))\\% where (info_min_time <= program_end AND info_max_time >= program_start) OR (program_start >= info_min_time and program_end <= info_max_time) \\% eval ad_group_id=adgroup_id\\% search (experience_id=\"ad\" OR experience_id=\"coupon\" OR experience_id=\"mfd\")  campaign_id=4669234d-e05a-459e-ab80-9919abd47bc3  (ad_group_id=\"*\")  country_code=USA\\% stats dc(external_id));    }");

	}

	public static String InputStoredTextVar1() throws IOException, InterruptedException {
		WebElement element = TORObj(); /* element.click(); */
		try { // driver.switchTo().activeElement().click();

			System.out.println("Started Executing InputSotordedVar1");

			Logs.Ulog(" Executing InputSotordedVar1 ");

			// driver.switchTo().activeElement().clear();

			driver.switchTo().activeElement().sendKeys(StoreTable.get("Var1").toString());

			System.out.println("EnTERED qUERY TEXT");
			return UpdateResult.Done();

		} catch (Throwable T) {
			System.out.println("Input error " + T.getMessage());
			return CatchStatementWebElement(T.getMessage());

		}

	}

	public static String SetAttributeSotordedVar1() throws IOException, InterruptedException {

		Logs.Ulog(" Executing SetAttributeSotordedVar1 ");

		try {
			WebElement Obj = TORObj();
			Logs.Ulog("SetAttribute on obj " + Obj.getText());

			js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0]." + TC.Param1 + "='" + StoreTable.get("Var1").toString() + "'",
					driver.switchTo().activeElement());

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			return CatchStatementWebElement(e.getMessage());
		}

	}
	/*
	 * public static String RoboMouseClickOnObj()
	 * 
	 * Logs.Ulog("Start of RoboMouseClickOnObj"); try {
	 * 
	 * C_RoboMouse.MoveMouseToWebEleCoord(TORObj());
	 * C_RoboMouse.robotPoweredClick();
	 * 
	 * return UpdateResult.Done();
	 * 
	 * } catch (Throwable e) { return CatchStatementWebElement(e.getMessage());
	 * }
	 * 
	 * }
	 * 
	 * }
	 */

	public static String AppendRandomPrefixString() throws IOException, InterruptedException {
		try {
			Logs.Ulog("AppendRandomPrefixString keyword is started");
			SecureRandom random = new SecureRandom();
			String randStr = new BigInteger(32, random).toString(16);
			StoreTable.put(TC.Param1, randStr + TC.InputData);
			emailid = randStr + TC.InputData;
			randStr = "";
			Logs.Ulog("InputData successfully prefixed with random String");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			StoreTable.put(TC.Param1, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String AppendRandomPwdPrefixString() throws IOException, InterruptedException {
		try {
			Logs.Ulog("AppendRandomPrefixString keyword is started");
			SecureRandom random = new SecureRandom();
			String randStr = new BigInteger(32, random).toString(16);
			StoreTable.put(TC.Param1, randStr + TC.InputData);
			// emailid = randStr+TC.InputData;
			randStr = "";
			Logs.Ulog("InputData successfully prefixed with random String");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			StoreTable.put(TC.Param1, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	private static String cint(String fscnum) {
		// TODO Auto-generated method stub
		return null;
	}

	private static void elseif(boolean b) {
		// TODO Auto-generated method stub

	}

	public static String OfferSum() throws IOException, InterruptedException {
		try {
			Logs.Ulog("AppendRandomPrefixString keyword is started");

			String a= TC.Param2;
			String b= TC.Param3;
			String c=	TC.Param4;
			
			String a1= a.replace("€", "");
			String a2= a1.replace(",", ".");
	
			String b1= b.replace("€", "");
			String b2= b1.replace(",", ".");
			
			String c1= c.replace("€", "");
			String c2= c1.replace(",", ".");
			
			float num_a= Float.parseFloat(a2);
			float num_b= Float.parseFloat(b2);
			float num_c= Float.parseFloat(c2);
			
			float result=num_a+num_b+num_c;
			
			double roundOff = (double) Math.round(result * 100) / 100;
			String str =Double.toString(roundOff);
			String sumvalue1 =str.replace(".0", "");
			String sumvalue =sumvalue1.replace(".", ",");
			

			StoreTable.put(TC.Param1,sumvalue);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			StoreTable.put(TC.Param1, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	// Used to truncate the Zero from Euro value : 6,70 --6,7
	// its take value as Inputdata and truncate it and stored in param1
	public static String EuroTruncateZero() throws IOException, InterruptedException {
		try {
			Logs.Ulog("TruncateZeroEuro");

			String abc = TC.InputData;

			if (abc.contains(",")) {
				if (abc.endsWith("0")) {
					abc = abc.substring(0, abc.length() - 1);
					System.out.println("abc"+abc);
				}
			}
			System.out.println("Not perform EuroTruncateZero");

			StoreTable.put(TC.Param1, abc);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			StoreTable.put(TC.Param4, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	/*
	 * public static String AccountNumberGenerator() throws IOException,
	 * InterruptedException { try{
	 * Logs.Ulog("AccountNumberGenerator keyword is started"); Long
	 * monoprix_Acc_No=(long) 9281072701234; WebElement Obj = TORObj(); do{
	 * 
	 * } return UpdateResult.UpdateStatus(); } catch (Throwable e) { //Updated
	 * by Avanish on 25/11/2016 StoreTable.put(TC.Param1,TC.InputData);
	 * Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
	 * return CatchStatementWebElement(e.getMessage()); } }
	 * 
	 * 
	 */

	// block method for testlink integration
	public static String checkBlockedStatus() throws IOException, InterruptedException {
		try {
			if (TC.failStepCtr > 0) {
				UpdateResult.GlobalStatus = TC.FAIL;
				TC.TestLinkStatus = TC.BLOCK;
				return TC.BLOCK;
			} else
				return TC.PASS;
		} catch (Throwable e) {
			return TC.BLOCK;
		}

	}

	// switch tabs

	public static String SendKeyCTRL_Tab() throws IOException, InterruptedException {

		try {
			Logs.Ulog("Executing SendKeyTab");
			HighlightElement();
			// DbClickJS();
			TORObj().sendKeys(Keys.chord(Keys.CONTROL + "\t"));
			// System.exit(0);
			return TC.PASS;
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}

	}

	// public static void ChangeTrailing4Digits() throws IOException,
	// InterruptedException {
	// String intFscNum = "";
	// try{
	// Logs.Ulog("ChangeTrailing4Digits keyword is started");
	// File file = new File(System.getProperty("user.dir"));
	// File parentFolder = file.getParentFile();
	// ExcelObj Global = new ExcelObj(parentFolder+ "/Projects/" +
	// "GlobalSetup.xlsm");
	// ExcelObj Resource = new ExcelObj(parentFolder+ "/Projects/CN/" +
	// "Resource_CN.xlsm");
	// String browser = Global.getCellData("GlobalSetup", "ConfigValue", 3);
	// String Fscnum = TC.InputData;
	// //int Fscnum1 = Integer.parseInt(Fscnum);
	// if (browser.equalsIgnoreCase("CHROME")){
	// intFscNum = Fscnum+"1";
	// }
	// else if (browser.equalsIgnoreCase("MOZILLA")){
	// intFscNum = Fscnum+"2";
	// }
	// else if (browser.equalsIgnoreCase("IE")){
	// intFscNum = Fscnum+"3";
	// }
	// String currentDir=System.getProperty("user.dir");
	// StoreTable.put(TC.Param1, intFscNum);
	// } catch (Throwable e) {
	// //Updated by test on 06/12/2016
	// //StoreTable.put(TC.Param1,TC.InputData);
	// Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
	// }
	// }

	public static String ChangeTrailing4Digits() throws IOException, InterruptedException {
		String intFscNum = "";
		try {
			Logs.Ulog("ChangeTrailing4Digits keyword is started");
			File file = new File(System.getProperty("user.dir"));
			File parentFolder = file.getParentFile();
			ExcelObj Global = new ExcelObj(parentFolder + "/Projects/" + "GlobalSetup.xlsm");
			ExcelObj Resource = new ExcelObj(parentFolder + "/Projects/CN/" + "Resource_CN.xlsm");
			String browser = Global.getCellData("GlobalSetup", "ConfigValue", 3);
			String Fscnum = TC.InputData;
			// int Fscnum1 = Integer.parseInt(Fscnum);
			if (browser.equalsIgnoreCase("CHROME")) {
				intFscNum = Fscnum + "3";
			} else if (browser.equalsIgnoreCase("MOZILLA")) {
				intFscNum = Fscnum + "2";
			} else if (browser.equalsIgnoreCase("IE")) {
				intFscNum = Fscnum + "3";
			}
			String currentDir = System.getProperty("user.dir");
			StoreTable.put(TC.Param1, intFscNum);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			// Updated by test on 06/12/2016
			// StoreTable.put(TC.Param1,TC.InputData);
			Logs.Ulog("Something went wrong with keyword: AppendRandomPrefixString");
			return TC.PASS;
		}
	}

	public static String ReturnCurrentDate() throws IOException, InterruptedException {
		Date date = new Date();
		Logs.Ulog("ReturnCurrentDate keyword is started");
		try {
			if (TC.InputData != null && !TC.InputData.trim().equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat(TC.InputData.trim());
				String formattedDate = sdf.format(date);
				StoreTable.put(TC.Param1, formattedDate);
				System.out.println(formattedDate);

			} else {
				Logs.Ulog("ReturnCurrentDate keyword is started with no arguements for format in input data");
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
				String formattedDate = sdf.format(date);
				StoreTable.put(TC.Param1, formattedDate);
				System.out.println(formattedDate);

			}
			Logs.Ulog("keyword: ReturnCurrentDate is Executed successfully!");
			return UpdateResult.UpdateStatus();
		} catch (Exception e) {
			// Updated by test on 06/12/2016
			System.out.println("Illegal arguement exception");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY");
			String formattedDate = sdf.format(date);
			System.out.println(formattedDate);
			StoreTable.put(TC.Param1, formattedDate);
			Logs.Ulog("Something went wrong with keyword: ReturnCurrentDate");
			return TC.PASS;
		}
	}

	public static String getFullImagePathFromTestData() throws IOException, InterruptedException {
		try {
			Logs.Ulog("uploadImageFromTestadata keyword is started");
			String currentDir = System.getProperty("user.dir");
			String inputDataPath = "";
			Path path = Paths.get(currentDir);
			inputDataPath = TC.TestDataPath + TC.InputData;

			StoreTable.put(TC.Param1, inputDataPath);

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			// Updated by test on 06/12/2016
			StoreTable.put(TC.Param1, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: getFullImagePathFromTestadata");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String inputTextFromParam1() throws IOException, InterruptedException {
		try {
			Logs.Ulog("inputTextFromParam1 keyword is started");
			TORObj().clear();
			TORObj().sendKeys(TC.TestDataPath + "receiptexample.jpeg");
			UpdateResult.ActualData = TC.Param1.trim();
			TC.ExpectedData = TC.Param1.trim();
			Logs.Ulog("InputText on obj Done ");
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			// Updated by test on 06/12/2016

			Logs.Ulog("Something went wrong with keyword: inputTextFromParam1");
			return CatchStatementWebElement(e.getMessage());
		}
	}

	public static String sendkeysctrs() throws IOException, InterruptedException {

		Logs.Ulog("Checking existance of element");
		try {

			Robot r = new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyPress(KeyEvent.VK_S);
			r.keyRelease(KeyEvent.VK_S);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(100);

			// rt.exec("d:\\print.exe")

		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}
		return ALM_UpdateStatus_HTMLAttchment;
	}

	public static String VerifyPdfTextContains() throws IOException, InterruptedException {

		try {

			PDDocument pddDocument = PDDocument.load(new File(TC.InputData), "UTF-8");
			PDFTextStripper textStripper = new PDFTextStripper();
			String content = textStripper.getText(pddDocument);

			// String nstr = new String ( dd);

			System.out.println(content);
			System.out.println(TC.ExpectedData);
			if (content.replaceAll("[^A-Za-z0-9]", "").replaceAll(" ", "")
					.contains((((TC.ExpectedData.replaceAll("[^A-Za-z0-9]", "").replaceAll(" ", "")))))) {

				UpdateResult.ActualData = content;
				TC.ExpectedData = content;
			} else {
				UpdateResult.ActualData = content;
				TC.ExpectedData = "Content is not macting ";
			}

		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}
		return UpdateResult.UpdateStatus();

	}

	public static String VerifySizeOfList() throws IOException, InterruptedException {

		try {

			Logs.Ulog("Executing CTRL A SortSizeofList");
			List<WebElement> EleCollection = WebElementsTORObj();
			int ItemList = EleCollection.size();

			UpdateResult.ActualData = String.valueOf(ItemList);
			TC.ExpectedData = String.valueOf(TC.ExpectedData);
			if (UpdateResult.ActualData.equals(TC.ExpectedData)) {
				UpdateResult.UpdateStatus();
				return TC.PASS;
			}
		} catch (Throwable t) {
			return CatchStatementWebElement(t.getMessage());
		}
		return ALM_UpdateStatus_HTMLAttchment;
	}

	public static String VerifypopupExist() throws IOException, InterruptedException {

		Logs.Ulog("Checking existance of element");
		try {
			if (TORObj().isDisplayed())
				;
			{
				driver.findElementById("cookieDisc_close").click();
				// Thread.sleep(5000);
			}
			return UpdateResult.Done();
		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}

	}

	public static String CloseAlertPresent() throws InterruptedException, IOException {

		try {

			// Check the presence of alert
			Alert alert = driver.switchTo().alert();
			// Alert present; set the flag
			// UpdateResult.ActualData = alert.getText();

			// if present consume the alert
			alert.accept();
			TimeUnit.SECONDS.sleep(3);
			// BrowserSync();
			// TC.ExpectedData = "Alert Should not popup";
			// return UpdateResult.UpdateStatus();
			return TC.PASS;

		} catch (NoAlertPresentException ex) {
			// Alert not present

			Logs.Ulog("Alert not present" + ex.getMessage());
			return CatchStatementWebElement(ex.getMessage());
		}

	}

	public static String deleteuser_api() throws IOException, InterruptedException {
		try {
			if (emailid != null) {
				Logs.Ulog("Delete user:" + emailid);
				File file = new File(System.getProperty("user.dir"));
				File parentFolder = file.getParentFile();
				ExcelObj Resource = new ExcelObj(parentFolder + "/Projects/CN/" + "Resource_CN.xlsm");
				String xmlfile = Resource.getCellData("TestData", "Datavalue", 31);
				String strpwd = Resource.getCellData("TestData", "Datavalue", 6);
				String currentDir = System.getProperty("user.dir");
				// String xmlpath = currentDir + xmlfile;
				String batchfile = currentDir + "\\RunAutomationScripts.bat";
				String SnappURL = Resource.getCellData("TestData", "Datavalue", 2);
				String partnerid = Resource.getCellData("TestData", "Datavalue", 32);
				String cmd = batchfile + " " + xmlfile + " " + emailid + " " + strpwd + " " + SnappURL + " "
						+ partnerid;
				Runtime.getRuntime().exec("cmd.exe /C start" + cmd);
				// int exitVal = process.waitFor();
				Logs.Ulog("Deleted user:" + emailid);
				// if (exitVal == 0) {
				// Logs.Ulog("Deleted user:" + emailid);
				// }

			} else {
				Logs.Ulog("No user registered to delete");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		emailid = "";
		return TC.PASS;

	}

	public static String deleteuser_ui() throws IOException, InterruptedException {
		try {
			if (emailid != null) {
				Logs.Ulog("Delete user:" + emailid);
				if (System.getProperty("excelFilePath") != null) {
					String filePath = System.getProperty("excelFilePath");
					ExcelObj Resource = new ExcelObj(filePath + "/Projects/CN/" + "Resource_CN.xlsm");
					String strpwd = Resource.getCellData("TestData", "Datavalue", 6);
					String SnappURL = Resource.getCellData("TestData", "Datavalue", 2);
					String currentDir = System.getProperty("user.dir");
					System.setProperty("webdriver.chrome.driver",
							filePath + "\\Supportingfile\\src\\main\\java\\chromedriver.exe");
					DesiredCapabilities capability = DesiredCapabilities.chrome();
					driver = new ChromeDriver();
					driver.manage().window().maximize();
					String envName = System.getProperty("environment");
					if (envName != null) {
						if (envName.equalsIgnoreCase("uat")) {

							driver.navigate().to("https://uat.cwallet.couponnetwork.fr");
						} else {
							driver.navigate().to(SnappURL);
						}
					} else
						driver.navigate().to(SnappURL);

				} else {
					File file = new File(System.getProperty("user.dir"));
					File parentFolder = file.getParentFile();
					ExcelObj Resource = new ExcelObj(parentFolder + "/Projects/CN/" + "Resource_CN.xlsm");
					// String xmlfile = Resource.getCellData("TestData",
					// "Datavalue", 31);
					String strpwd = Resource.getCellData("TestData", "Datavalue", 6);
					String SnappURL = Resource.getCellData("TestData", "Datavalue", 2);
					// String partnerid = Resource.getCellData("TestData",
					// "Datavalue", 32);
					String currentDir = System.getProperty("user.dir");
					// String xmlpath = currentDir + xmlfile;
					System.setProperty("webdriver.chrome.driver",
							System.getProperty("user.dir") + "\\src\\main\\java\\chromedriver.exe");
					DesiredCapabilities capability = DesiredCapabilities.chrome();
					driver = new ChromeDriver();
					driver.manage().window().maximize();
					String envName = System.getProperty("environment");
					if (envName != null) {
						if (envName.equalsIgnoreCase("uat")) {
							driver.navigate().to("https://uat.cwallet.couponnetwork.fr");

						} else {
							driver.navigate().to(SnappURL);
						}
					} else
						driver.navigate().to(SnappURL);
					// TimeUnit.SECONDS.sleep(5);
				}

				driver.findElementById("login").sendKeys("srnagara");
				driver.findElementById("password").sendKeys("Catalina@123");
				driver.findElementByXPath("//input[@name='commit']").click();
				TimeUnit.SECONDS.sleep(5);
				if (driver.findElementByXPath("//a[@href='/logout']").isDisplayed()) {
					driver.findElementByXPath("//a[text()='Members']").click();
					 TimeUnit.SECONDS.sleep(5);
					driver.findElementById("filter_email").sendKeys(emailid);
					driver.findElementByXPath("//input[@value='Filter']").click();
					TimeUnit.SECONDS.sleep(5);
					if (driver.findElementByXPath("//table[@class='onable odded center']//tr[2]/td[3]").isDisplayed()) {
						String stremail = driver
								.findElementByXPath("//table[@class='onable odded center']//tr[2]/td[3]").getText()
								.trim();
						if (emailid.equals(stremail)) {
							driver.findElementByXPath("//a[@href='#delete_member']").click();
							TimeUnit.SECONDS.sleep(2);
							CloseAlertPresent();
							if (driver.findElementByXPath("//a[@href='#delete_member']").isDisplayed()) {
								Logs.Ulog("Issue with deletion" + emailid);
								driver.findElementByXPath("//a[@href='#delete_member']").click();
								TimeUnit.SECONDS.sleep(2);
								CloseAlertPresent();
							} else {
								Logs.Ulog("Deleted user:" + emailid);
							}
							driver.findElementByXPath("//a[@href='/logout']").click();
							TimeUnit.SECONDS.sleep(2);
						} else {
							Logs.Ulog("No user present to be selected");
						}
					} else {
						Logs.Ulog("No user present to be selected");
					}
				} else {
					Logs.Ulog("Login to Snapp failed");
				}
				// if (exitVal == 0) {
				// Logs.Ulog("Deleted user:" + emailid);
				// }

			} else {
				Logs.Ulog("No user registered to delete");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Logs.Ulog("Issue with deletion" + emailid + e.getMessage());
		}
		// driver.close();
		driver.quit();
		emailid = "";
		return TC.PASS;

	}

	public static String acceptPrintPopUp() throws IOException, InterruptedException, AWTException {
		if ((driver instanceof ChromeDriver && StoreTable.get("Execute ON Browser").equals("Chrome"))) {
			Logs.Ulog("Working on GC browser");
			Robot robo = new Robot();
			robo.keyPress(KeyEvent.VK_CONTROL);
			Thread.sleep(500);
			robo.keyPress(KeyEvent.VK_SHIFT);
			Thread.sleep(500);
			robo.keyPress(KeyEvent.VK_P);
			Thread.sleep(2000);
			robo.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(500);
			robo.keyRelease(KeyEvent.VK_SHIFT);
			Thread.sleep(500);
			robo.keyRelease(KeyEvent.VK_P);
			Thread.sleep(2000);
		}
		try {
			Runtime.getRuntime().exec("./payloads/AcceptForprintCoupon.exe");
			return TC.PASS;
		} catch (IOException e) {
			Logs.Ulog("Exception in acceptPrintPopUp - " + e);
		}
		return TC.FAIL;
	}

	public static String waitForDOMLoad() throws InterruptedException {
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(driver, 40);
		try {
			wait.until(new Function<WebDriver, Boolean>() {

				@Override
				public Boolean apply(WebDriver dr) {
					js = (JavascriptExecutor) dr;
					return js.executeScript("return document.readyState").equals("complete");
				}
			});
			return TC.PASS;
		} catch (Exception e) {
			return TC.FAIL;
		}

	}

	public static String Stringtrim_Split() throws IOException, InterruptedException {

		Logs.Ulog("Checking existance of element");
		try {

			String obtained = TC.InputData.trim();
			System.out.println("obtained is - " + obtained);
			String[] arr = obtained.split("& ");
			System.out.println("arr is - " + arr[0]);
			StoreTable.put(TC.ExpectedData, arr[0] + arr[1]);
			return UpdateResult.Done();

		} catch (Throwable e) {
			UpdateResult.ActualData = TC.FAIL;
			TC.FailDescription = e.getMessage();
			UpdateResult.UpdateStatus();
			return TC.FAIL;
		}
		// return ALM_UpdateStatus_HTMLAttchment;
	}

	public static String lowercaseEmail() throws IOException, InterruptedException {
		try {

			String obtained = TC.InputData.trim();
			System.out.println("obtained is - " + obtained);

			StoreTable.put(TC.Param1, TC.InputData.toLowerCase());
			emailid = TC.InputData.toLowerCase();

			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {

			StoreTable.put(TC.Param1, TC.InputData);
			Logs.Ulog("Something went wrong with keyword: lowerstring");
			return CatchStatementWebElement(e.getMessage());
		}

	}
	public static String clickEscInPrintPopup() throws AWTException, InterruptedException, IOException
	{try {
		Thread.sleep(3000);
		Robot robot=new Robot();
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(3000);
		robot.keyPress(KeyEvent.VK_ENTER);
		return UpdateResult.UpdateStatus();
	}
	catch(Throwable e)
	{
		Logs.Ulog("Something went wrong with keyword: clickEscInPrintPopup");
		return CatchStatementWebElement(e.getMessage());

	}
	}
	public static String VerifyBonusIsNotAdded() throws IOException, InterruptedException {

		try {
		String a=TC.InputData;
		String b=TC.ExpectedData;
			String a1= a.replace("€", "");
			String a2= a1.replace(",", ".");
			String a3=a2.replaceAll("\\W", "");
			String b1=b.replace("€", "");
			String b2=b1.replace(",", ".");
			String b3=b2.replaceAll("\\W", "");
			
			if (a3.equals(b3)) {
                UpdateResult.ActualData = a3 + " - Equal  - " + b3;
                TC.ExpectedData = a3 + " - Equal  - " + b3;
                return UpdateResult.UpdateStatus();
          } else {
                UpdateResult.ActualData = a3 + " - Equal - " + b3;
                TC.ExpectedData = a3 + " - Not Equal - " + b3;
                return UpdateResult.UpdateStatus();
          }


		} catch (Throwable e) {

			return CatchStatementWebElement(e.getMessage());
		}

	}
	public static String elementNotClickable() throws IOException, InterruptedException {

		try {
			TORObj().click();
			UpdateResult.ActualData = String.valueOf(false);
			TC.ExpectedData = String.valueOf(TC.InputData);
			return UpdateResult.UpdateStatus();
		} catch (Throwable e) {
			UpdateResult.ActualData = "TRUE";
			TC.ExpectedData = TC.InputData;
			return UpdateResult.UpdateStatus();
		}
	}
}
