package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Item {

	
	String description;
	String result;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Item(String description, String result) {
		this.description = description;
		this.result = result;
	}
	
	public Item() {
		// TODO Auto-generated constructor stub
	}
}
