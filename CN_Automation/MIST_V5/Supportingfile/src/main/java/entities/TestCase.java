package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TestCase {

	
	String name;
	Project project;
	TestSteps testSteps;
	TestPlans testPlans;
	String description;
	String tags;
	Priority priority;
	Comments comments;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public TestSteps getTestSteps() {
		return testSteps;
	}

	public void setTestSteps(TestSteps testSteps) {
		this.testSteps = testSteps;
	}

	public TestPlans getTestPlans() {
		return testPlans;
	}

	public void setTestPlans(TestPlans testPlans) {
		this.testPlans = testPlans;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public TestCase() {
		// TODO Auto-generated constructor stub
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public Comments getComments() {
		return comments;
	}

	public void setComments(Comments comments) {
		this.comments = comments;
	}
	
	

}
