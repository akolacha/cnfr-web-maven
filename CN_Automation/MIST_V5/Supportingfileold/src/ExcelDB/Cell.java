package ExcelDB;


/**
 * Represents a cell with row and column specific value.
 * @author ShivaPrakash
 *
 */


public class Cell {
	
	protected Cell(int row, int col, String colName, Object value){
		m_col = col;
		m_row = row;
		m_colName = colName;
		m_value = value;
	}
	
	/**
	 * Returns the row number for the current cell.
	 * @return integer
	 */
	public int getRow(){
		return m_row;
	}
	
	/**
	 * Returns the column for the current cell.
	 * @return integer
	 */
	public int getColumn(){
		return m_col;
	}
	
	/**
	 * Gets the column name for the current cell.
	 * @return String
	 */
	public String getColumnName(){
		return m_colName;
	}
	
	/**
	 * Returns the value for the current cell.
	 * @return Object
	 */
	public Object getValue(){
		return m_value;
	}
	
	int m_row, m_col;
	String m_colName;
	Object m_value;
}
