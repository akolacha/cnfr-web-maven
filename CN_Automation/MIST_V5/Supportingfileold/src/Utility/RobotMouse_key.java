package Utility;

import java.awt.AWTException;

import java.awt.Robot;

import java.awt.event.InputEvent;

import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;
import static Utility.FunctionLibrary.driver;
import org.openqa.selenium.WebElement;

public class RobotMouse_key {

	private static Robot mouseObject;



	public static JavascriptExecutor executor;

	public RobotMouse_key(WebDriver driver) throws AWTException {

		this.mouseObject = new Robot();
		

		this.executor = (JavascriptExecutor) driver;

	}

	public void MouseRelease() {

		mouseObject.mouseRelease(0);

	

	}


	public void MoveMouseToAbsCoord(int xCoordinates, int yCoordinates) {

		mouseObject.mouseMove(xCoordinates, yCoordinates);

		mouseObject.waitForIdle();

	}

	public void MoveMouseToCoordOnPg(int xCoordinates, int yCoordinates)

	{

		Logs.Ulog("Executing MoveMouseToCoordOnPg");

		// Get Browser dimensions

		int browserWidth = driver.manage().window().getSize().width;

		int browserHeight = driver.manage().window().getSize().height;

		// Get dimensions of the window displaying the web page

		int pageWidth = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientWidth").toString());

		int pageHeight = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientHeight").toString());

		// Calculate the space the browser is using for toolbars

		int browserFurnitureOffsetX = browserWidth - pageWidth;

		int browserFurnitureOffsetY = browserHeight - pageHeight;

		// Calculate the correct X/Y coordinates based upon the browser
		// furniture offset and the position of the browser on the desktop

		int xPosition = driver.manage().window().getPosition().x
				+ browserFurnitureOffsetX + xCoordinates;

		int yPosition = driver.manage().window().getPosition().y
				+ browserFurnitureOffsetY + yCoordinates;

		// Move the mouse to the calculated X/Y coordinates

		mouseObject.mouseMove(xPosition, yPosition);

		mouseObject.waitForIdle();

		Logs.Ulog("--- End of  MoveMouseToCoordOnPg----");

	}

	public static void MoveMouseToWebEleCoord(WebElement element) {

		// Get Browser dimensions

		Logs.Ulog("Executing MoveMouseToWebEleCoord ");

		int browserWidth = driver.manage().window().getSize().width;

		int browserHeight = driver.manage().window().getSize().height;

		// Get dimensions of the window displaying the web page

		int pageWidth = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientWidth").toString());

		int pageHeight = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientHeight").toString());

		// Calculate the space the browser is using for toolbars

		int browserFurnitureOffsetX = browserWidth - pageWidth;

		int browserFurnitureOffsetY = browserHeight - pageHeight;

		// Get the coordinates of the WebElement on the page and calculate the
		// centre point

		int webElementX = element.getLocation().x
				+ Math.round(element.getSize().width / 2);

		int webElementY = element.getLocation().y
				+ Math.round(element.getSize().height / 2);

		// Calculate the correct X/Y coordinates based upon the browser
		// furniture offset and the position of the browser on the desktop

		int xPosition = driver.manage().window().getPosition().x
				+ browserFurnitureOffsetX + webElementX;

		int yPosition = driver.manage().window().getPosition().y
				+ browserFurnitureOffsetY + webElementY;

		// Move the mouse to the calculated X/Y coordinates

		mouseObject.mouseMove(xPosition, yPosition);
	
		mouseObject.waitForIdle();

		Logs.Ulog("----End of  MoveMouseToWebEleCoord ---- ");

	}

//#################################################################################################
	
	public static void MouseDownToWebEleCoord(WebElement element) {

		// Get Browser dimensions

		Logs.Ulog("Executing MoveMouseToWebEleCoord ");

		int browserWidth = driver.manage().window().getSize().width;

		int browserHeight = driver.manage().window().getSize().height;

		// Get dimensions of the window displaying the web page

		int pageWidth = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientWidth").toString());

		int pageHeight = Integer.parseInt(executor.executeScript(
				"return document.documentElement.clientHeight").toString());

		// Calculate the space the browser is using for toolbars

		int browserFurnitureOffsetX = browserWidth - pageWidth;

		int browserFurnitureOffsetY = browserHeight - pageHeight;

		// Get the coordinates of the WebElement on the page and calculate the
		// centre point

		int webElementX = element.getLocation().x
				+ Math.round(element.getSize().width / 2);

		int webElementY = element.getLocation().y
				+ Math.round(element.getSize().height / 2);

		// Calculate the correct X/Y coordinates based upon the browser
		// furniture offset and the position of the browser on the desktop

		int xPosition = driver.manage().window().getPosition().x
				+ browserFurnitureOffsetX + webElementX;

		int yPosition = driver.manage().window().getPosition().y
				+ browserFurnitureOffsetY + webElementY;

		// Move the mouse to the calculated X/Y coordinates

		mouseObject.mouseMove(xPosition, yPosition);

		mouseObject.waitForIdle();

		Logs.Ulog("----End of  MoveMouseToWebEleCoord ---- ");

	}

	
	
//#################################################################################################
	
	public void robotPoweredMoveMouseToAbsoluteCoordinates(int xCoordinates,
			int yCoordinates) {

		mouseObject.mouseMove(xCoordinates, yCoordinates);

		mouseObject.waitForIdle();

	}

	public static void robotPoweredMouseDown() {

		mouseObject.mousePress(InputEvent.BUTTON1_DOWN_MASK);

		mouseObject.waitForIdle();

	}

	public static void robotPoweredMouseUp() {

		mouseObject.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

		mouseObject.waitForIdle();

	}

	public static void robotPoweredClick() {

		mouseObject.mousePress(InputEvent.BUTTON1_DOWN_MASK);

		mouseObject.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);

		mouseObject.waitForIdle();

	}

}
