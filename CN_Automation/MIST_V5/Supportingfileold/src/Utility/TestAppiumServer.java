package Utility;

import static Utility.R_Start.Appiumserver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class TestAppiumServer {
	 
	 String appiumInstallationDir = "C:/Program Files (x86)";// e.g. in Windows
	 //String appiumInstallationDir = "/Applications";// e.g. for Mac
	
	 
	 public TestAppiumServer() {
	  File classPathRoot = new File(System.getProperty("user.dir"));
	  String osName = System.getProperty("os.name");
	 
	  if (osName.contains("Windows")) {
		  Appiumserver = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
	     .usingDriverExecutable(new File(appiumInstallationDir + File.separator + "Appium" + File.separator + "node.exe"))
	     .withAppiumJS(new File(appiumInstallationDir + File.separator + "Appium" + File.separator
	       + "node_modules" + File.separator + "appium" + File.separator + "bin" + File.separator + "appium.js"))
	     .withIPAddress("127.0.0.1")
       .usingPort(4723)  //usingAnyFreePort()
       .withStartUpTimeOut(30, TimeUnit.SECONDS));
	   
	   
	 
	  } else if (osName.contains("Mac")) {
		  Appiumserver = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
	     .usingDriverExecutable(new File(appiumInstallationDir + "/Appium.app/Contents/Resources/node/bin/node"))
	     .withAppiumJS(new File(
	       appiumInstallationDir + "/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js"))
	     .withLogFile(new File(new File(classPathRoot, File.separator + "log"), "androidLog.txt")));
	 
	  } else {
	   // you can add for other OS, just to track added a fail message
	   //Assert.fail("Starting appium is not supporting the current OS.");
	  }
	 }
	 
	 /**
	  * Starts appium server
	  */
	 public void startAppiumServer() {
		 Appiumserver.start();
	 }
	 
	 /**
	  * Stops appium server
	  */
	 public void stopAppiumServer() {
		 Appiumserver.stop();
	 }
}
