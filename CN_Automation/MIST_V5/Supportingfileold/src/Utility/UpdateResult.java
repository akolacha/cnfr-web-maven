/*
 ********************************************************************************
 ********************************************************************************
//  TimeTrax Automation Selenium
//  Start date 7 Jan 2014
//  Author : Raghavendra Pai & Santhosha HC
//  Test :    Update Result script V 1.9 

********************************************************************************
********************************************************************************/


package Utility;
import static Utility.R_Start.Resultdata;
import static Utility.R_Start.StoreTable;
import static Utility.R_Start.Testrow;
import static Utility.R_Start.cap;
import static Utility.ResultExcel.SLNOCol;
import static Utility.ResultExcel.MODULENAMECol;
import static Utility.ResultExcel.NoofTestCasesPassedCol;
import static Utility.ResultExcel.NoofTestCasesFailedCol;
import static Utility.ResultExcel.ExecutionTimeCol;
import static Utility.ResultExcel.DefectIDCol;
import static Utility.ResultExcel.NotesCol;
import static Utility.R_Start.totalTime;
import static Utility.R_Start.FinaltotalTime;
import static Utility.R_Start.cap;
import static Utility.ALM.ALMFilepath;
import static Utility.FunctionLibrary.driver;
import static Utility.R_Start.S_Obj;
import static Utility.GmailSendMail.SendmailWrite;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteWebDriver;

import sun.util.logging.resources.logging;

import com.thoughtworks.selenium.Selenium;
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium;
public class UpdateResult extends ResultExcel  {

	

	public static String ExpectedData;
	public static String ActualData;
	public static String PassDescription;
	public static String FailDescription;
	public static String TestDescription;
	public static String ResultSheet ;
	public static String GlobalStatus  = "PASS";
	public static String FinalStatus = "PASS";
	public static String ModuleFinalStatus = "PASS";
	
	//public static int   rownum = 1;
	public static String  TestIDCol = "TestID" ;
	public static String  ModuleNameCol = "ModuleName";
	public static String RequirementsCol = "Requirements";
	public static String  KeywordCol = "Keyword";
	public static String  TestDescCol = "TestDescription";
	//public static String  AutomationDescCol = "PassDescription" ;
	public static String  PassDescCol  = "PassDescription" ;
	public static String  FailDescCol  = "FailDescription" ;
	public static String  ExpectedCol = "Expected" ;
	public static String  ActualCol  = "Actual" ;
	public static String StatusCol  = "Status" ;
	public static String CaptureScreenCol  = "ScreenShot" ;
	
	public static int TestCasePassed  = 0 ;
	public static int TestCaseFailed  = 0 ;
	public static int TotalTestCasePassed  = 0 ;
	public static int TotalTestCaseFailed  = 0 ;
	
	public static int ModuleTestCasePassed  = 0 ;
	public static int ModuleTestCaseFailed  = 0 ;
	public static int ModuleTotalTestCasePassed  = 0 ;
	public static int ModuleTotalTestCaseFailed  = 0 ;
	public static int  SLNO = 1;	
	public static String browserName;
	public static String os ;
	public static String version;
	
	public static String Done() throws IOException
	{
		
		if (StoreTable.get("GenerateReport").toString().equalsIgnoreCase("YY"))
		{
			if (StoreTable.get("GenerateHTMLReport").toString().equalsIgnoreCase("YY"))
				HTMLResults.UpdateHtmlDone();
			
				
			try
			{
				if (StoreTable.get("GeneratePDFReport").toString().equalsIgnoreCase("YY"))
					HTMLResults.UpdatePDFStatusDone();
			}
			catch(Throwable t)
			{
				Logs.Ulog("Error While Generating PDF report");
			}
			
			
			if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
			{
				
				
				Resultdata.setCellData(ResultSheet , TestDescCol, ResultExcel.rownum, TC.TestDescription);			
			    Resultdata.setCellData(ResultSheet ,  StatusCol , ResultExcel.rownum, "DONE" );
			
				 if (R_Start.StoreTable.get("FormatReport").toString().equalsIgnoreCase("YY"))
				  Resultdata.setRowColor(ResultSheet ,  RequirementsCol , ResultExcel.rownum , "Green");
			 
				 ResultExcel.rownum++;
			}
		}	
		return TC.PASS;
	}
	public static String UpdateStatus() throws IOException, InterruptedException 
	{
		if (StoreTable.get("GenerateReport").toString().equalsIgnoreCase("YY"))
		{	
			
			
			if(TC.ExpectedData.trim().equalsIgnoreCase(ActualData.trim())) 
			{
				
			////FunctionLibrary.PassHighlightElement();
				
				Logs.Ulog("Start write result ");
			    captureEntirePageScreenshotEachStep();
				Logs.Ulog("^^^^^^^^^^^^^^^^^^Start html write result ^^^^^^^^^^^^^^^^^^^^");
				if (StoreTable.get("GenerateHTMLReport").toString().equalsIgnoreCase("YY"))
					HTMLResults.UpdateHtmlStatusPass();
				
				try
				{
					if (StoreTable.get("GeneratePDFReport").toString().equalsIgnoreCase("YY"))
						HTMLResults.UpdatePDFStatusPass();
				}
				catch(Throwable t)
				{
					Logs.Ulog("Error While Generating PDF report");
				}
				
				Logs.Ulog("^^^^^^^^^^^^^^^^^^End html write result ^^^^^^^^^^^^^^^^^^^^");	
				
				if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
				{
					
					Resultdata.setCellData(ResultSheet , TestDescCol, ResultExcel.rownum, TC.TestDescription);
					Resultdata.setCellData(ResultSheet , PassDescCol, ResultExcel.rownum, TC.PassDescription);
					Resultdata.setCellData(ResultSheet , ExpectedCol, ResultExcel.rownum, TC.ExpectedData);
					Resultdata.setCellData(ResultSheet ,  ActualCol , ResultExcel.rownum, ActualData  );
					Resultdata.setCellData(ResultSheet ,  StatusCol , ResultExcel.rownum, "PASS" );
						
					//TestNgReportsForDashboard.TestNgSteps.add(TC.TCID  +":"+ TC.TestDescription +" "+ TC.PassDescription +" "+ TC.ExpectedData +" "+ ActualData +" : "+ "PASS");
				
				   if (R_Start.StoreTable.get("FormatReport").toString().equalsIgnoreCase("YY"))					 
				     Resultdata.setRowColor(ResultSheet ,  RequirementsCol , ResultExcel.rownum , "Green");
				   ResultExcel.rownum++;
					
				} // end of generateexcelrepot 
				
				 
				 TC.TestDescription = "";
				 TC.PassDescription  = "";
				 TC.ExpectedData = "";
				 ActualData = "";
				 Logs.Ulog("End write result ");
				 TestCasePassed++;
				 TotalTestCasePassed++;
				
				 return TC.PASS;	
			}
			else
			{	Logs.Ulog("Start write result ");
			try
			{
				FunctionLibrary.ErrorHighlightElement();
				captureEntirePageScreenshotOnerror();
			}
			catch(Throwable T)
			{
				Logs.Ulog("Error while highlight and taking Screenshot "+T.getMessage());
			}
			 
			 
			 PrograssBar.DriverProgressBar.setForeground(Color.RED);
			 PrograssBar.TestProgressBar.setForeground(Color.RED);
			Logs.Ulog("^^^^^^^^^^^^^^^^^^Start html write result ^^^^^^^^^^^^^^^^^^^^");
					if (StoreTable.get("GenerateHTMLReport").toString().equalsIgnoreCase("YY"))
						HTMLResults.UpdateHtmlStatusFail();
					
				
					try
					{
						if (StoreTable.get("GeneratePDFReport").toString().equalsIgnoreCase("YY"))
							HTMLResults.UpdatePDFStatusFAIL();
					}
					catch(Throwable t)
					{
						Logs.Ulog("Error While Generating PDF report");
					}	
					
					
			Logs.Ulog("^^^^^^^^^^^^^^^^^^End html write result ^^^^^^^^^^^^^^^^^^^^");		
			
				if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
				{
					Resultdata.setCellData(ResultSheet , TestDescCol, ResultExcel.rownum, TC.TestDescription);
					Resultdata.setCellData(ResultSheet , FailDescCol, ResultExcel.rownum, TC.FailDescription);
					Resultdata.setCellData(ResultSheet ,  ExpectedCol, ResultExcel.rownum, TC.ExpectedData);
					Resultdata.setCellData(ResultSheet ,  ActualCol , ResultExcel.rownum, ActualData  );
					Resultdata.setCellData(ResultSheet ,  StatusCol , ResultExcel.rownum, "FAIL" );
					TestNgReportsForDashboard.TestNgSteps.add(TC.TCID  +":"+ TC.TestDescription +" "+ TC.PassDescription +" "+ TC.ExpectedData +" "+ ActualData +" : " + "FAIL");
				
					
					
					 if (R_Start.StoreTable.get("FormatReport").toString().equalsIgnoreCase("YY"))
						 Resultdata.setRowColor(ResultSheet ,  RequirementsCol , ResultExcel.rownum , "red");
					 
					 ResultExcel.rownum++;
				}// end of repotexcel
				
				
				 TC.TestDescription = "";
				 TC.PassDescription  = "";
				 TC.ExpectedData = "";
				 ActualData = "";			
				 Logs.Ulog("End write result ");
				 TestCaseFailed++;
				 TotalTestCaseFailed++;
				 GlobalStatus = TC.FAIL;
				 FinalStatus = TC.FAIL;
				 ModuleFinalStatus = TC.FAIL;
				
				return TC.FAIL;
			}
			
		}
		
		return "Done";
	}
	
	
	public static void UpDateModule() throws IOException
	{

		if (StoreTable.get("GenerateReport").toString().equalsIgnoreCase("YY"))
		{
			
			if (StoreTable.get("GenerateHTMLReport").toString().equalsIgnoreCase("YY"))
				HTMLResults.UpdateHtmlmodule();
			
			
			if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
			{
				Logs.Ulog("Start UpDateModule into result ");
				Resultdata.setCellData(ResultSheet ,  TestIDCol , ResultExcel.rownum, TC.TCID );
				Resultdata.setCellData(ResultSheet ,  ModuleNameCol , ResultExcel.rownum, TC.Module );
				Resultdata.setCellData(ResultSheet ,  RequirementsCol , ResultExcel.rownum, TC.Requirements );
				
				if (R_Start.StoreTable.get("FormatReport").toString().equalsIgnoreCase("YY"))
				    Resultdata.setRowColor(ResultSheet ,  RequirementsCol , ResultExcel.rownum , "blue");
				ResultExcel.rownum++;		
			}
			Logs.Ulog("End UpDateModule into result ");
		}
	}
	
	/*
	public static void UpDateModule()
	{
		Resultdata.setCellData(ResultSheet ,  ModuleNameCol , ResultExcel.rownum, TC.Module );
		Resultdata.setCellData(ResultSheet ,  RequirementsCol , ResultExcel.rownum, TC.Requirements );
		ResultExcel.rownum++;
	}*/
	
	public static void UpDateKeyword()
	{

		if (StoreTable.get("GenerateReport").toString().equalsIgnoreCase("YY"))
		{
			Resultdata.setCellData(ResultSheet ,  TestIDCol , ResultExcel.rownum, TC.TCID );
			Resultdata.setCellData(ResultSheet ,  KeywordCol , ResultExcel.rownum, TC.Keyword +"_"+Testrow );
		//ResultExcel.rownum++;
		}
	}
	
	public static void UpDateSummary() throws IOException
	{
		Logs.Ulog("Start  UpDateSummary into result ");
		HTMLResults.UpdateMainReport();
		
		
		//MsgBody = MsgBody+ "\"& vbNewLine &\"" +Integer.toString(SLNO) +"---"+TC.TestModuleName +"---"+Integer.toString(TestCasePassed ) +"---"+Integer.toString(TestCaseFailed )+"---"+Long.toString(totalTime)+"";
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.SLNOCol , ResultExcel.SummarySheetrownum, Integer.toString(SLNO) );
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.MODULENAMECol , ResultExcel.SummarySheetrownum, TC.TestModuleName );
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.NoofTestCasesPassedCol , ResultExcel.SummarySheetrownum, Integer.toString(TestCasePassed ));
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.NoofTestCasesFailedCol , ResultExcel.SummarySheetrownum, Integer.toString(TestCaseFailed ));
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.ExecutionTimeCol , ResultExcel.SummarySheetrownum,Long.toString(totalTime) );
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.DefectIDCol , ResultExcel.SummarySheetrownum, "" );
		Resultdata.ReslutSummarySetRowColor("Summary", 0, ResultExcel.SummarySheetrownum, "yellow");
		ResultExcel.SummarySheetrownum++;		
		SLNO++;
		Logs.Ulog("End  UpDateSummary into result ");
		
	}
	
	public static void UpDateTotalSummary()
	{
		Logs.Ulog("Start  total UpDateSummary into result ");
		try
		{
			
		HTMLResults.UpdateTotalMainReport();
		HTMLResults.UpdateModuleMainReport();
		
		Resultdata.ReslutSetCellData("Summary" , ResultExcel.SLNOCol , ResultExcel.SummarySheetrownum, "Total");
		Resultdata.ReslutSetCellData("Summary" , ResultExcel.MODULENAMECol , ResultExcel.SummarySheetrownum, "" );
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.NoofTestCasesPassedCol , ResultExcel.SummarySheetrownum, Integer.toString(TotalTestCasePassed ));
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.NoofTestCasesFailedCol , ResultExcel.SummarySheetrownum, Integer.toString(TotalTestCaseFailed ));
		Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.ExecutionTimeCol , ResultExcel.SummarySheetrownum,Long.toString(FinaltotalTime) );
		Resultdata.ReslutSummarySetRowColor("Summary", 0, ResultExcel.SummarySheetrownum, "green");
		//Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.DefectIDCol , ResultExcel.SummarySheetrownum, "" );
		
		ResultExcel.SummarySheetrownum++;		
		SLNO++;
		Logs.Ulog("End  total UpDateSummary into result ");
		}
		catch (Exception e) {
			Logs.Ulog("Error - While Updating summary");
		}
		   
		  
		   
	}
	
	public static String UpDateBrowserVer()
	{       S_Obj = new WebDriverBackedSelenium(driver, "http://URL");
		    cap =  ((RemoteWebDriver)  driver).getCapabilities(); 
			browserName = cap.getBrowserName().toString().toUpperCase();
			version = cap.getVersion().toString(); 
			   
		    Resultdata.ReslutSetCellData("Summary" ,   ResultExcel.MODULENAMECol , 0, browserName +"_Ver_" + version );
		  //  MsgBody = MsgBody + "\"& vbNewLine & \"" + browserName +"_Ver_" + version+ "---Test Summary---Test Case Passed---"+StoreTable.get("Build Version_Description");
		 //   MsgBody = MsgBody +  "\"& vbNewLine & \"SL NO---MODULE NAME---No of Test Cases Passed---No of Test Cases Failed---Execution Time in Sec\"";
			return TC.PASS;
	}
	
	

	public static void captureEntirePageScreenshotOnerror() throws IOException{
        // take screen shots
		
		String Cnt = null;
		
		if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
		{
				 Cnt = ((IOSDriver) driver).getContext();
		}	
		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
		{
				 Cnt = ((AndroidDriver) driver).getContext();
		}
		 
		try
		{
			
			 
		
        if(StoreTable.get("CaptureScreeShotOnError").toString().equalsIgnoreCase("YY"))
        {
        	 if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
	    		{
				   ((IOSDriver) driver).context("NATIVE_APP");
	    		}	
	    		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
	    		{
	    			((AndroidDriver) driver).context("NATIVE_APP");
	    		}
        	Logs.Ulog("---Start CaptureScreeShotOnError---");          
        	//FunctionLibrary.ErrorHighlightElement();
        	   date = new Date();
        	   //StandardLibrary.BrowserZoom50Per();
        	
        	   
        	SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
            String formattedDate = sdf.format(date);         
                        File scrFile = ((TakesScreenshot)((WebDriver) driver)).getScreenshotAs(OutputType.FILE);
                        CurrentFilepath =    ResImageFolderPath+"\\"+ TC.TCID +"_"+formattedDate +".png" ;
                     //   CurrentFilepath =    ResImageFolderPath+"\\tt.jpg" ;
            FileUtils.copyFile(scrFile, new File(CurrentFilepath));
            if (StoreTable.get("DesktopScreeShotOnError").toString().equalsIgnoreCase("YY"))
            {
          	  DesktopScreenCapture(CurrentFilepath);
            }
            
            //upload alm results
            
            if(StoreTable.get("ALM_CaptureScreeShotOnError").toString().equalsIgnoreCase("YY"))
            {
	            try
				{		 
					     new  ALM().ALMStepScreenAttach();				  
						 Process p = Runtime.getRuntime().exec("C:\\windows\\SysWOW64\\cscript.exe " + ALMFilepath);						
				}
				catch(Throwable t)
				{
					Logs.Ulog("Error Alm cobnfigutration failures please update with proper alm configurations");
				}
            }
            
            if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))			
            	Resultdata.setCellData(ResultSheet,CaptureScreenCol,ResultExcel.rownum, "ClickToviewScreen", CurrentFilepath);
            
            Logs.Ulog("---End  CaptureScreeShotOnError---");       
            //StandardLibrary.BrowserZoom100Per();
            if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
    		{
    				 ((IOSDriver)  driver).context(Cnt);
    		}	
    		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
    		{
    			((AndroidDriver) driver).context(Cnt);
    		}
            
        }
		}
		
		catch(Exception T)
		{
			Logs.Ulog("---Error Unable to Find the Browser ");
			
			   if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
	    		{
	    				 ((IOSDriver)  driver).context(Cnt);
	    		}	
	    		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
	    		{
	    			((AndroidDriver) driver).context(Cnt);
	    		}
		}
	}
	
	public static void captureEntirePageScreenshotEachStep() throws IOException{
        // take screen shots
		
	String Cnt = null;
		
		if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
		{
			
				try {
					Cnt = ((IOSDriver) driver).getContext();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				 
		}	
		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
		{
				 try {
					Cnt = ((AndroidDriver) driver).getContext();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		 
		try
		{
		if(StoreTable.get("CaptureScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
        {
			//FunctionLibrary.ErrorHighlightElement();             // capturescreen
			   if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
	    		{
				   try {
					((IOSDriver) driver).context("NATIVE_APP");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		}	
	    		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
	    		{
	    			try {
						((AndroidDriver) driver).context("NATIVE_APP");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		}
			
		    
        	Logs.Ulog("---Start CaptureScreeShotOnEachStep---"); 
        	if (TC.FAIL.contains("FAIL"))
        		//StandardLibrary.BrowserZoom50Per();
        	//driver.context("Native_App");
        	   date = new Date();
        	
        	SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
            String formattedDate = sdf.format(date);         
                        File scrFile = ((TakesScreenshot)((WebDriver) driver)).getScreenshotAs(OutputType.FILE);
                        CurrentFilepath =    ResImageFolderPath+"\\"+ TC.TCID +"_"+formattedDate +".png" ;
                        //ALMCurrentFilepath =    ResImageFolderPath+"\\"+ TC.TCID+"_"+formattedDate.replace("-", "").replace(" ", "") +".jpg" ;
                       // CurrentFilepath =    ResImageFolderPath+"\\tt.jpg" ;
            FileUtils.copyFile(scrFile, new File(CurrentFilepath));
          if (StoreTable.get("DesktopScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
          {
        	  DesktopScreenCapture(CurrentFilepath);
          }
            
            //upload alm results
                  
            if(StoreTable.get("ALM_CaptureScreeShotOnEachStep").toString().equalsIgnoreCase("YY"))
            {
	            try
				{		 
					     new  ALM().ALMStepScreenAttach();				  
						Process p = Runtime.getRuntime().exec("C:\\windows\\SysWOW64\\cscript.exe " + ALMFilepath);						
				}
				catch(Throwable t)
				{
					Logs.Ulog("Error Alm configutration failures please update with proper alm configurations");
				}
            }
            
            
            if (StoreTable.get("GeneratExcelReport").toString().equalsIgnoreCase("YY"))
			    Resultdata.setCellData(ResultSheet,CaptureScreenCol,ResultExcel.rownum, "ClickToviewScreen", CurrentFilepath);
            
            Logs.Ulog("---End  CaptureScreeShotOnEachStep---");   
            if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
    		{
    				 ((IOSDriver)  driver).context(Cnt);
    		}	
    		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
    		{
    			((AndroidDriver) driver).context(Cnt);
    		}
     
           // StandardLibrary.BrowserZoom100Per();
        }
        
	}
	
	catch(Exception T)
	{
		Logs.Ulog("---Error Unable to Find the Browser ");
		   if(StoreTable.get("Execute ON Browser").equals("PerfectoIOSDevice"))
   		{
   				 ((IOSDriver)  driver).context(Cnt);
   		}	
   		if(StoreTable.get("Execute ON Browser").equals("PerfectoAndroidDevice") || StoreTable.get("Execute ON Browser").equals("AndroidEmulator") || StoreTable.get("Execute ON Browser").equals("AndroidLocalDevice"))
   		{
   			((AndroidDriver) driver).context(Cnt);
   		}

	}
}

	public static void DesktopScreenCapture(String filenamepath) throws IOException,
    AWTException {

Toolkit toolkit = Toolkit.getDefaultToolkit();
Dimension screenSize = toolkit.getScreenSize();
Rectangle screenRect = new Rectangle(screenSize);
Robot robot = new Robot();
BufferedImage image = robot.createScreenCapture(screenRect);
ImageIO.write(image, "png", new File(filenamepath));

}

	
}
