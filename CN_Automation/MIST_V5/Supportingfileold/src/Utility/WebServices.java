package Utility;



import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.exec.util.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.rest.RestishHandler;

import com.eviware.soapui.impl.WsdlInterfaceFactory;
import com.eviware.soapui.impl.rest.mock.RestMockRequest;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlOperation;
import com.eviware.soapui.impl.wsdl.WsdlProject;
import com.eviware.soapui.impl.wsdl.WsdlRequest;
import com.eviware.soapui.impl.wsdl.WsdlSubmit;
import com.eviware.soapui.impl.wsdl.WsdlSubmitContext;

import com.jayway.restassured.path.xml.XmlPath;
import static Utility.R_Start.StoreTable;
import static Utility.TC.WebDriverMock;
import OR_TestParm.OR;
import com.eviware.soapui.model.iface.Response;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
//Author Santhosha HC
//Date 19 March 2015
//Last updated by santhosha
//Date 19 june March 2016
public class WebServices {

	public static String[] keys = null;	
	public static String[] expvalues = null;	
	private static  WsdlProject  myWEBs;
	public WebServices() throws InterruptedException {
		super();
		if (StoreTable.get("EnableProxy").equals("YY"))
		{
		        System.getProperties().put("http.proxyHost", StoreTable.get("http_proxyHost"));
		        System.getProperties().put("http.proxyPort", StoreTable.get("http_proxyPort"));
		        System.getProperties().put("https.proxyHost", StoreTable.get("https_proxyHost"));
		        System.getProperties().put("https.proxyPort", StoreTable.get("https_proxyPort"));
		}
		// TODO Auto-generated constructor stub
	}
	


	public static String WebServiceGet() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGet");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					if (TC.Param1.toLowerCase().contains("text"))
					{
						
							
							
							UpdateResult.ActualData = Resp;
					}
					else
					{
						
						UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");	
					}
					
					
			}						
					
			        StoreVarbyMethodName (Resp);
					return	UpdateResult.UpdateStatus();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	public static String WebServiceGetStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGetStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					if (TC.Param1.toLowerCase().contains("text"))
					{
						
							
							
							UpdateResult.ActualData = Resp;
					}
					else
					{
						
						UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");	
					}
					
					
			}
			
			        StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
			        StoreVarbyMethodName (Resp);
			        
					return	UpdateResult.Done();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	
	
	public static String WebServiceGetContains() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServiceGetContains");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).asString());
				
			}
			else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .get(new URL(TC.TestObjects)).asString());
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
				if (Resp.contains(TC.ExpectedData))
				{
					
					UpdateResult.ActualData = TC.ExpectedData;
				}
					
			}						
			StoreVarbyMethodName (Resp);
					return	UpdateResult.UpdateStatus();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

	
	public static String WebServicePost() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePost");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			
			
			HashMap<String, String> headermapbody = new HashMap<>();
			String[] headerarrbody = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
			headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, "::");
		
			
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
				
			}
			else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString());
				
				Resp=  String.valueOf(WebDriverMock.given().log().all().formParameters(headermapbody).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString());
			}
			
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}			
					StoreVarbyMethodName (Resp);
					return	UpdateResult.UpdateStatus();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		 }
	}

	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	

	
		public static String WebServicePostWithFormParameters() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServicePost");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				
				
				HashMap<String, String> headermapbody = new HashMap<>();
				String[] headerarrbody = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarrbody = FunctionLibrary.getStringAsArray(TC.Param2);
				headermapbody = FunctionLibrary.getMapFromArray(headerarrbody, "::");
			
				
				String Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString());
					
				}
				else if( TC.Param1.isEmpty() && !TC.Param2.isEmpty())
				{
					Resp=  String.valueOf(WebDriverMock.given().log().all().body(headermapbody).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());
					
					Resp=  String.valueOf(WebDriverMock.given().log().all().formParameters(headermapbody).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(WebDriverMock.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .post(new URL(TC.TestObjects)).asString());
				}
				
				
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
				Logs.Ulog(Resp);
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
				
				String[] actArr = null;
				ArrayList<String> actValues = null;
				if (Resp.contains("<html"))
				{
				Document d = Jsoup.parse(Resp);
//				String resptemp = d.body().getElementsByTag("h1").text();
				UpdateResult.ActualData = d.body().getElementsByTag("b").text();
				
				}	
				else
				{
					
				
				if (TC.Param1.contains("application/json"))
				{
					JsonPath resp = new JsonPath(Resp);
					actValues = new ArrayList<>();
					
					
					
						if(keys.length == expvalues.length){
							for (int i = 0; i < keys.length; i++) {
								Logs.Ulog(resp.getString(keys[i]));
								actValues.add(resp.getString(keys[i]));
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
						}
						else{
							Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							
						}
				}	
						if (TC.Param1.contains("application/xml"))
						{
							XmlPath Xmlresp = new XmlPath(Resp);
							 actValues = new ArrayList<>();
										
								if(keys.length == expvalues.length){
									for (int i = 0; i < keys.length; i++) {
										Logs.Ulog(Xmlresp.getString(keys[i]));
										actValues.add(Xmlresp.getString(keys[i]));
									}
								}
								else{
									Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
								
						}
						UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
				}			
						StoreVarbyMethodName (Resp);
						return	UpdateResult.UpdateStatus();
				
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			 }
		}

	
	
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	public static String WebServicePostStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePostStoreKey ");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			
			String Resp= null;
			
			if(TC.Param2.isEmpty())
			{
				 Resp=  WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).asString();
			}
			else
			{
				Resp=  WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .post(new URL(TC.TestObjects)).asString();
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}		
			 
			   StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
			   StoreVarbyMethodName (Resp);
			   return	UpdateResult.Done();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		 }
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH	
	
	
	public static String WebServicePut() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePut");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).getStatusCode());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{   
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).getStatusCode());
				 
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .put(new URL(TC.TestObjects)).asString());
				
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}						
					StoreVarbyMethodName (Resp);
					return	UpdateResult.UpdateStatus();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePutStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePutStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).getStatusCode());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{   
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().put(new URL(TC.TestObjects)).getStatusCode());
				 
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .put(new URL(TC.TestObjects)).asString());
				
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}						
			   StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
			   StoreVarbyMethodName (Resp);
			   return	UpdateResult.Done();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePatch() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePatch");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .patch(new URL(TC.TestObjects)).asString());
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}						
					StoreVarbyMethodName (Resp);
					return	UpdateResult.UpdateStatus();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
	public static String WebServicePatchStoreKey() throws IOException, InterruptedException 
	{
		Logs.Ulog("Verifying the WebServicePatchStoreKey");
		try {
			
			HashMap<String, String> headermap = new HashMap<>();
			String[] headerarr = null;
			keys = FunctionLibrary.getStringAsArray(TC.InputData);
			expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
			headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
			headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
			String Resp= null;
			
			if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 
				 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
			}
			else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
			{
				 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().patch(new URL(TC.TestObjects)).asString());
				
			}
			else
			{
				Resp=  String.valueOf(WebDriverMock.given().log().all()
		                   .headers(headermap)
		                   .body(TC.Param2).relaxedHTTPSValidation().when()
		                   .patch(new URL(TC.TestObjects)).asString());
			}
			
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
			Logs.Ulog(Resp);
			Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
			String[] actArr = null;
			ArrayList<String> actValues = null;
			if (Resp.contains("<html"))
			{
			Document d = Jsoup.parse(Resp);
//			String resptemp = d.body().getElementsByTag("h1").text();
			UpdateResult.ActualData = d.body().getElementsByTag("b").text();
			
			}	
			else
			{
				
			
			if (TC.Param1.contains("application/json"))
			{
				JsonPath resp = new JsonPath(Resp);
				actValues = new ArrayList<>();
				
				
				
					if(keys.length == expvalues.length){
						for (int i = 0; i < keys.length; i++) {
							Logs.Ulog(resp.getString(keys[i]));
							actValues.add(resp.getString(keys[i]));
						}
						
						actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
					
					}
					else{
						Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
						
					}
			}	
					if (TC.Param1.contains("application/xml"))
					{
						XmlPath Xmlresp = new XmlPath(Resp);
						 actValues = new ArrayList<>();
									
							if(keys.length == expvalues.length){
								for (int i = 0; i < keys.length; i++) {
									Logs.Ulog(Xmlresp.getString(keys[i]));
									actValues.add(Xmlresp.getString(keys[i]));
								}
							}
							else{
								Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
							
					}
					UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
			}						

StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
   StoreVarbyMethodName (Resp);
   return	UpdateResult.Done();
			
		}

		catch (Exception e)
		{
			    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
		}
		
			
	}
	
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
	
		public static String WebServiceDelete() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServiceDelete");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				String Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(WebDriverMock.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .delete(new URL(TC.TestObjects)).asString());
				}
				
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
				Logs.Ulog(Resp);
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
				String[] actArr = null;
				ArrayList<String> actValues = null;
				if (Resp.contains("<html"))
				{
				Document d = Jsoup.parse(Resp);
//				String resptemp = d.body().getElementsByTag("h1").text();
				UpdateResult.ActualData = d.body().getElementsByTag("b").text();
				
				}	
				else
				{
					
				
				if (TC.Param1.contains("application/json"))
				{
					JsonPath resp = new JsonPath(Resp);
					actValues = new ArrayList<>();
					
					
					
						if(keys.length == expvalues.length){
							for (int i = 0; i < keys.length; i++) {
								Logs.Ulog(resp.getString(keys[i]));
								actValues.add(resp.getString(keys[i]));
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
						}
						else{
							Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							
						}
				}	
						if (TC.Param1.contains("application/xml"))
						{
							XmlPath Xmlresp = new XmlPath(Resp);
							 actValues = new ArrayList<>();
										
								if(keys.length == expvalues.length){
									for (int i = 0; i < keys.length; i++) {
										Logs.Ulog(Xmlresp.getString(keys[i]));
										actValues.add(Xmlresp.getString(keys[i]));
									}
								}
								else{
									Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
								
						}
						UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
				}						
						StoreVarbyMethodName (Resp);
						return	UpdateResult.UpdateStatus();
				
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}
			
				
		}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
		
		public static String WebServiceDeleteStoreKey() throws IOException, InterruptedException 
		{
			Logs.Ulog("Verifying the WebServiceDeleteStoreKey");
			try {
				
				HashMap<String, String> headermap = new HashMap<>();
				String[] headerarr = null;
				keys = FunctionLibrary.getStringAsArray(TC.InputData);
				expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
				headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
				headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
				String Resp= null;
				
				if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 
					 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
				}
				else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
				{
					 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString());
					
				}
				else
				{
					Resp=  String.valueOf(WebDriverMock.given().log().all()
			                   .headers(headermap)
			                   .body(TC.Param2).relaxedHTTPSValidation().when()
			                   .delete(new URL(TC.TestObjects)).asString());
				}
				
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
				Logs.Ulog(Resp);
				Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
				String[] actArr = null;
				ArrayList<String> actValues = null;
				if (Resp.contains("<html"))
				{
				Document d = Jsoup.parse(Resp);
//				String resptemp = d.body().getElementsByTag("h1").text();
				UpdateResult.ActualData = d.body().getElementsByTag("b").text();
				
				}	
				else
				{
					
				
				if (TC.Param1.contains("application/json"))
				{
					JsonPath resp = new JsonPath(Resp);
					actValues = new ArrayList<>();
					
					
					
						if(keys.length == expvalues.length){
							for (int i = 0; i < keys.length; i++) {
								Logs.Ulog(resp.getString(keys[i]));
								actValues.add(resp.getString(keys[i]));
							}
							
							actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
						
						}
						else{
							Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
							
						}
				}	
						if (TC.Param1.contains("application/xml"))
						{
							XmlPath Xmlresp = new XmlPath(Resp);
							 actValues = new ArrayList<>();
										
								if(keys.length == expvalues.length){
									for (int i = 0; i < keys.length; i++) {
										Logs.Ulog(Xmlresp.getString(keys[i]));
										actValues.add(Xmlresp.getString(keys[i]));
									}
								}
								else{
									Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
								}
								
								actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
							
								
						}
						UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
				}						

				   StoreTable.put(TC.ExpectedData, UpdateResult.ActualData);
				   StoreVarbyMethodName (Resp);
				   return	UpdateResult.Done();
				
			}

			catch (Exception e)
			{
				    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
			}
			
				
		}
	//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH		
		
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				
				public static String WebServiceSoap() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceSoap");
					try {
						 
						myWEBs  = new WsdlProject();
						
						// import amazon wsdl
					
						WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(myWEBs, TC.TestObjects , true )[0];
						//WsdlInterface wsdl = WsdlInterfaceFactory.importWsdl(project, "https://localhost/retail/retail.php?wsdl", true )[0];
						
						
						// get desired operation
					
						//WsdlOperation op = wsdl.getOperationAt(0);
						WsdlOperation op = wsdl.getOperationByName(TC.Param1);
						//WsdlOperation op = wsdl.getOperationByName("GetCityForecastByZIP");

						String reqContent = op.createRequest(true);
						//reqContent = replaceNode ( reqContent) ;
						//reqContent = reqContent.replaceAll("ZIP\\>\\?" , "ZIP\\>94701");
						//reqContent = reqContent.replaceAll("ToCurrency\\>\\?" , "ToCurrency\\>USD");
						
						String k = TC.InputData;
						
						HashMap SOAPKeys =   new HashMap();
						String a[] = k.split(";");
						
						for(int sk=0; sk<a.length;sk++)
						{
							String[] SplitedKey =  a[sk].split(":~");
							SOAPKeys.put(SplitedKey[0], SplitedKey[1]);
						}
						

						reqContent = new UpdateXmlValue().updateXML(reqContent, SOAPKeys);
						
						WsdlRequest req = op.addNewRequest("myReq");
						System.out.println(reqContent);
						req.setRequestContent(reqContent);
						
						WsdlSubmitContext wsdlSubmitContext = new WsdlSubmitContext(req);

						WsdlSubmit<?> submit = (WsdlSubmit<?>) req.submit(wsdlSubmitContext, false);

						Response response = submit.getResponse();

						String result = response.getContentAsString();

						Logs.Ulog("The result ="+result);

						Diff diff = XMLUnit.compareXML(result, TC.ExpectedData); 
						if(diff.identical())			
						{
							UpdateResult.ActualData = TC.ExpectedData; 
						}else{
							UpdateResult.ActualData = result; 
						}
						StoreVarbyMethodName (response.toString());
					      return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}

				//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceStoreSessionID() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceDelete");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						String Resp= null;
						
						if(TC.Param2.isEmpty())
						{
							 Resp=  WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).asString();
						}
						else
						{
							Resp=  WebDriverMock.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .delete(new URL(TC.TestObjects)).asString();
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						String[] actArr = null;
						ArrayList<String> actValues = null;
						if (Resp.contains("<html"))
						{
						Document d = Jsoup.parse(Resp);
//						String resptemp = d.body().getElementsByTag("h1").text();
						UpdateResult.ActualData = d.body().getElementsByTag("b").text();
						
						}	
						else
						{
							
						
						if (TC.Param1.contains("application/json"))
						{
							JsonPath resp = new JsonPath(Resp);
							actValues = new ArrayList<>();
							
							
							
								if(keys.length == expvalues.length){
									for (int i = 0; i < keys.length; i++) {
										Logs.Ulog(resp.getString(keys[i]));
										actValues.add(resp.getString(keys[i]));
									}
									
									actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
								
								}
								else{
									Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
									
								}
						}	
								if (TC.Param1.contains("application/xml"))
								{
									XmlPath Xmlresp = new XmlPath(Resp);
									 actValues = new ArrayList<>();
												
										if(keys.length == expvalues.length){
											for (int i = 0; i < keys.length; i++) {
												Logs.Ulog(Xmlresp.getString(keys[i]));
												actValues.add(Xmlresp.getString(keys[i]));
											}
										}
										else{
											Logs.Ulog("!!!ERROR!!!: The KEYS & EXPECTED VALUES are not equal");
										}
										
										actArr = Arrays.copyOf(actValues.toArray(), actValues.toArray().length,String[].class);
									
										
								}
								UpdateResult.ActualData = FunctionLibrary.getArrayAsString(actArr,";;");
						}						
						StoreVarbyMethodName (Resp);
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}	
				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServicePostVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServicePostVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						String Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().post(new URL(TC.TestObjects)).getStatusCode());
							 
							 
						}
						else
						{
							Resp=  String.valueOf(WebDriverMock.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .post(new URL(TC.TestObjects)).getStatusCode());
							
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
								
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}
				//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceGetVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceGetVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						String Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().get(new URL(TC.TestObjects)).getStatusCode());
							 
						}
						else
						{
							Resp=  String.valueOf(WebDriverMock.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .get(new URL(TC.TestObjects)).getStatusCode());
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
							
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}
				
				
				

				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				public static String WebServiceDeleteVerifyStatusCode() throws IOException, InterruptedException 
				{
					Logs.Ulog("Verifying the WebServiceDeleteVerifyStatusCode");
					try {
						
						HashMap<String, String> headermap = new HashMap<>();
						String[] headerarr = null;
						keys = FunctionLibrary.getStringAsArray(TC.InputData);
						expvalues = FunctionLibrary.getStringAsArray(TC.ExpectedData);
						headerarr = FunctionLibrary.getStringAsArray(TC.Param1);
						headermap = FunctionLibrary.getMapFromArray(headerarr, ":");
						String Resp= null;
						
						
						if(TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 
							 Resp=  String.valueOf(WebDriverMock.given().log().all().relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).getStatusCode());
						}
						else if( !TC.Param1.isEmpty() && TC.Param2.isEmpty())
						{
							 Resp=  String.valueOf(WebDriverMock.given().log().all().headers(headermap).relaxedHTTPSValidation().when().delete(new URL(TC.TestObjects)).getStatusCode());
							 
						}
						else
						{
							Resp=  String.valueOf(WebDriverMock.given().log().all()
					                   .headers(headermap)
					                   .body(TC.Param2).relaxedHTTPSValidation().when()
					                   .delete(new URL(TC.TestObjects)).getStatusCode());
						}
						
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------Start of Responce String ------HHHHHHHHHHHHHHHHHH");
						Logs.Ulog(Resp);
						Logs.Ulog("HHHHHHHHHHHHHHHHHH--------End of Responce String ------HHHHHHHHHHHHHHHHHH");
						
						UpdateResult.ActualData = Resp;	
						TC.ExpectedData  = Round(TC.ExpectedData );
						StoreVarbyMethodName (Resp);
							
								return	UpdateResult.UpdateStatus();
						
					}

					catch (Exception e)
					{
						    return FunctionLibrary.CatchStatementWebElement(e.getMessage());
					}
					
						
				}				
				
//HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
				
				public static String Round( String val)
				{
					
					try
					{
						Logs.Ulog("Start Round the string function ");
						double angle = Double.parseDouble(val);
					    DecimalFormat df = new DecimalFormat("#");
					    String angleFormated = df.format(angle);
					    return angleFormated;
					}
					catch (Throwable t)
					{
						Logs.Ulog("Error while  Round the string function " + t.getMessage());
					}
					
					return val;
					
					
				}
				
					
				
				public static String StoreVarbyMethodName(String Retval )
				{
				
					 try 
					 {
						 Throwable t = new Throwable(); 
					      StackTraceElement[] elements = t.getStackTrace(); 
					      String Cmn = elements[1].getMethodName();
					      StoreTable.put(Cmn, Retval); 
					 }
					 catch( Throwable T)
					 {
						 Logs.Ulog("Error while  StoreVarbyMethodName  the string function " + T.getMessage());
					 }
			       
			      
				   return Retval;
			      
				}
				
}
